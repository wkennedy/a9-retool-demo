# A9 Retool Demo

![](retool.png)

This is a snapshot of the Retool demo app I put together for a LinkedIn article.


The app was also submitted to Retool for the Showcase, since no other Jira applications came up when I searched.

It's basic and is not much more than a proof of concept.

William
